import pandas
import numpy as np

def logistic(x):
    return 1.0/(1 + np.exp(-x))

def logistic_deriv(x):
    return logistic(x) * (1 - logistic(x))

LR = 1
TF = 0.7

full_data = pandas.read_csv('MLP_data.csv', ';').dropna().reset_index(drop=True)

drop_columns = ['output', 'date', 'time', 'ISO_DateTime_UTC', 'pressure_gradient', 'water_level']
I_dim = full_data.shape[1] - len(drop_columns)
H_dim = 4


epoch_count = 10

# np.random.seed(1)
weights_ItoH = np.random.uniform(-1, 1, (I_dim, H_dim))
weights_HtoO = np.random.uniform(-1, 1, H_dim)

preActivation_H = np.zeros(H_dim)
postActivation_H = np.zeros(H_dim)


entries_count = full_data.shape[0]
training_count = (int)(entries_count*TF)
chosen_idx = np.random.choice(entries_count, size = training_count, replace=False) 

training_data = full_data.iloc[chosen_idx].reset_index(drop=True)
target_output = training_data.output
training_data = training_data.drop(drop_columns, axis=1)
training_data = np.asarray(training_data)

validation_data = full_data.drop(chosen_idx).reset_index(drop=True)
validation_output = validation_data.output
validation_data = validation_data.drop(drop_columns, axis=1)
validation_count = validation_data.shape[0]
validation_data = np.asarray(validation_data)

#####################
#training
#####################
for epoch in range(epoch_count):
    for sample in range(training_count):
        for node in range(H_dim):
            preActivation_H[node] = np.dot(training_data[sample,:], weights_ItoH[:, node])
            postActivation_H[node] = logistic(preActivation_H[node])
            
        preActivation_O = np.dot(postActivation_H, weights_HtoO)
        postActivation_O = logistic(preActivation_O)
        
        FE = postActivation_O - target_output[sample]
        
        for H_node in range(H_dim):
            S_error = FE * logistic_deriv(preActivation_O)
            gradient_HtoO = S_error * postActivation_H[H_node]
                       
            for I_node in range(I_dim):
                input_value = training_data[sample, I_node]
                gradient_ItoH = S_error * weights_HtoO[H_node] * logistic_deriv(preActivation_H[H_node]) * input_value
                
                weights_ItoH[I_node, H_node] -= LR * gradient_ItoH
                
            weights_HtoO[H_node] -= LR * gradient_HtoO

#####################
#validation
#####################            
correct_classification_count = 0
for sample in range(validation_count):
    for node in range(H_dim):
        preActivation_H[node] = np.dot(validation_data[sample,:], weights_ItoH[:, node])
        postActivation_H[node] = logistic(preActivation_H[node])
            
    preActivation_O = np.dot(postActivation_H, weights_HtoO)
    postActivation_O = logistic(preActivation_O)
        
    if postActivation_O > 0.5:
        output = 1
    else:
        output = 0     
        
    if output == validation_output[sample]:
        correct_classification_count += 1

print('Percentage of correct classifications:')
print(correct_classification_count*100/validation_count)